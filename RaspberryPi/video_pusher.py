
import requests, datetime

def push_video(pi_id, date, time, video_location):
	with open(video_location, 'rb') as video_data:
		vf = video_data.read()
		url = "http://localhost:51237//api//Video//?pi_key=" + pi_id
		url += '&indate=' + date + '&intime=' + time
		request = requests.post(url, data = bytearray(vf))

	print(request.status_code)
	return request.status_code == 200	# Let the calling function know this was succesful

print(push_video('test_pi', datetime.date.today().strftime("%B %d, %Y"), datetime.datetime.now().time().strftime("%H:%M:%S"), 'C:\\Users\\Trey\\Downloads\\SampleVideo_1280x720_1mb.mp4'))
