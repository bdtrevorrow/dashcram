﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DashCRAMWebApp.Account;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;

namespace DashCRAMWebApp.Tests
{
    [TestClass]
    public class AutomatedTests
    {

        [TestMethod]
        public void TestLogin() {
            Assert.IsTrue(Login.ValidateLogin("test", "Tester@1234"));
            Assert.IsFalse(Login.ValidateLogin("fake", "FakeLogin@1234"));
            Assert.IsFalse(Login.ValidateLogin("", ""));
        }

        [TestMethod]
        public void TestUserNameValidation() {
            Assert.IsTrue(Validate.UserName("test")); // Valid user name -- should return true
            Assert.IsFalse(Validate.UserName("iDoNotExist"));    // invalid user name -- should return false
        }

        [TestMethod]
        public void TestPiValidation() {
            Assert.IsTrue(Validate.PiRegKey("test_pi")); // valid pi ID -- should return true
            Assert.IsFalse(Validate.PiRegKey("6543210"));       // invalid pi ID -- should return false
        }

        [TestMethod]
        public void TestIsPiTaken() {
            Assert.IsTrue(Validate.IsPiTaken("test_pi")); // Pi should have a registered user
            Assert.IsFalse(Validate.IsPiTaken("123456")); // Pi should not have a registered user
        }

        [TestMethod]
        public void TestMakingNewAccount() {

            Assert.IsTrue(DbLevelAccountManager.AddAccountToDb("x12x23x45", "cKelly", "password"));

            // removing new account created by the test from the db

            // resetting cam table
            using (var DbContext = new dashcramdbEntities()) {
                var result = DbContext.cams.SingleOrDefault(b => b.pi_id == "x12x23x45");
                if (result != null) {
                    result.username = null;
                    DbContext.SaveChanges();
                }
            }

            // resetting account table
            using (var DbContext = new dashcramdbEntities()) {
                var result = DbContext.accounts.SingleOrDefault(b => b.username == "cKelly");
                DbContext.Entry(result).State = System.Data.Entity.EntityState.Deleted;
                DbContext.SaveChanges();
            }
        }

        [TestMethod]
        public void TestResetPassword() {
            // If it is, reset the password to modified
            DbLevelAccountManager.ResetPwd("test", "modified");

            // Make sure it was modified, then reset it to what it was before to end the test
            using (var db = new dashcramdbEntities()) {
                Assert.IsTrue(db.accounts.Find("test").password == "modified");
                db.accounts.Find("test").password = "Tester@1234";
                db.SaveChanges();
            }
        }

        [TestMethod]
        public void TestVideoRetrieval() {
            // The list returned should never be null
            Assert.IsNotNull(DbLevelAccountManager.RetrieveUserVideos("test").Count);

        }

        [TestMethod]
        public void TestRemovePiFromAccount() {

            Assert.IsTrue(DbLevelAccountManager.RemovePiFromAccount("test", "test_pi"));

            // Re-assigns the user to the pi after the test is completed
            using (var db = new dashcramdbEntities()) {

                db.cams.Find("test_pi").username = "test";
                db.SaveChanges();
            }
        }

        [TestMethod]
        public void TestChangeVideoName() {
            // Tests happy path and then resets the video name
            Assert.IsTrue(DbLevelAccountManager.ChangeVideoName("test", "test.mp4",
                          "BillyJoBob'sWedding.mp4", "\\..\\..\\..\\DashCRAMWebApp\\Content\\Videos\\"));
            DbLevelAccountManager.ChangeVideoName("test", "BillyJoBob'sWedding.mp4",
                                                  "test.mp4", "\\..\\..\\..\\DashCRAMWebApp\\Content\\Videos\\");
            // Tests if the method works when that video doesn't exist
            Assert.IsFalse(DbLevelAccountManager.ChangeVideoName("test", "fake.mp4",
                                                    "modified.mp4", "\\..\\..\\..\\DashCRAMWebApp\\Content\\Videos\\"));
        }


        public void TestTaggingVidsToFolders() {

            // Creating Test Data
            int[] psudoList = new int[2];
            psudoList[0] = 1;
            psudoList[1] = 6; // 1 and 6 are currently the only rows in the table

            String folderName = "MyTestFolder"; // Test Folder name for the Method

            // Testing Method
            Assert.IsTrue(DbLevelAccountManager.TagVidsToFolder(psudoList.ToList(), folderName));

            // Cleaning up the mess in the database
            using (var db = new dashcramdbEntities()) {
                foreach (var row in db.videos) { // doing it like this bc it takes less thinking on my part
                    if (row.video_folder == "MyTestFolder") {
                        row.video_folder = null;
                    }
                }
                db.SaveChanges();
            }
        }

        [TestMethod]
        public void TestVideoRemoval()
        {
            //Create Test File
            string file_location = "\\..\\..\\..\\DashCRAMWebApp\\Content\\Videos\\";
            File.Create(AppDomain.CurrentDomain.BaseDirectory + file_location + "test\\" + "testtoremove.mp4").Close();
            File.SetAttributes(AppDomain.CurrentDomain.BaseDirectory + file_location + "test\\" + "testtoremove.mp4", FileAttributes.Normal);
            
            //Remove Test Video From Table
            using (var db = new dashcramdbEntities())
            {
                var testtoremove = new video();
                testtoremove.username = "test";
                testtoremove.video_name = "testtoremove.mp4";
                testtoremove.date = new DateTime(999999);
                testtoremove.time = new TimeSpan(1, 1, 1);
                db.videos.Add(testtoremove);
                db.SaveChanges();
                Assert.IsTrue(DbLevelAccountManager.RemoveVideo(
                    db.videos.Where(x => x.video_name == "testtoremove.mp4").FirstOrDefault().id, 
                    file_location));
            }
        }

        [TestMethod]
        public void TestAddingNewPi() {

            String fakeUserName = "Zeke";
            String fakePiID = "Bocephus";

            Assert.IsTrue(DbLevelAccountManager.AddPiToExisingAccount(fakeUserName, fakePiID)); // Running Method
            Assert.IsTrue(Validate.PiAccount(fakePiID, fakeUserName)); // Checking The Method Did What It Should Do with a known working method
            DbLevelAccountManager.RemovePiFromAccount(fakeUserName, fakePiID); // Resetting Db

        }
    }
}             
    
