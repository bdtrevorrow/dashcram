﻿<%@ Page Title="View Video" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewVideo.aspx.cs" Inherits="DashCRAMWebApp.ViewVideo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <video id="videoplayer" width = "640" height="360" controls>
                <source runat="server" id="video_source" type="video/mp4" />
            </video>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="editTitleButton" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <br />

    <asp:TextBox runat="server" ID="video_title"></asp:TextBox>
    <asp:Label runat="server">.mp4</asp:Label>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Button runat="server" ID="editTitleButton" OnClick="editTitleButton_Click" value="Update Name" Text="Update Name"/>
        </ContentTemplate>
    </asp:UpdatePanel>

    <br />

    <p>Date: <span runat="server" id="video_date"></span></p>
    <p>Time: <span runat="server" id="video_time"></span></p>

</asp:Content>

