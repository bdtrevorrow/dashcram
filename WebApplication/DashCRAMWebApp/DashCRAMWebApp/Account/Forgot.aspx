﻿<%@ Page Title="Forgot password" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Forgot.aspx.cs" Inherits="DashCRAMWebApp.Account.ForgotPassword" Async="true" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <style>

        /*hide validation message fields*/

        #message, #message3 {

            display: none;
        }

        /* Add a green text color and a checkmark when the requirements are right */
        .valid {
            color: green;
        }

            .valid:before {
                position: relative;
                left: -35px;
                content: "✔";
            }

        /* Add a red text color and an "x" when the requirements are wrong */
        .invalid {
            color: red;
        }

            .invalid:before {
                position: relative;
                left: -35px;
                content: "✖";
            }
    </style>
    <script src="../Scripts/validate.js"></script>

    <div class="row">
        <div class="col-md-8">
            <asp:PlaceHolder id="loginForm" runat="server">
                <div class="form-horizontal">
                    <h4>Forgot your password?</h4>
                    <hr />
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Username</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Email" ClientIDMode="Static" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="The username field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Registration Key</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="RegistrationKey" ClientIDMode="Static" CssClass="form-control"  />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="RegistrationKey"
                                CssClass="text-danger" ErrorMessage="The Registration Key field is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Password" ClientIDMode="Static" CssClass="col-md-2 control-label">New Password</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Password" ClientIDMode="Static" TextMode="Password" CssClass="form-control" />
                
                        </div>
                    </div>
                    <div id="message">
                        <h3>Password must contain the following:</h3>
                        <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                        <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                        <p id="number" class="invalid">A <b>number</b></p>
                        <p id="length" class="invalid">Minimum <b>8 characters</b></p>
                    </div>
                     <div id="message3">
                        <h3>Registration key must contain the following:</h3>
                        <p id="id" class="invalid">Must be a valid Raspberry Pi ID</p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <asp:Button runat="server" ID="SubmitButton" ClientIDMode="Static" OnClick="Forgot" Text="Submit" CssClass="btn btn-default" Disabled="true"/>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="DisplayEmail" Visible="false">
                <p class="text-info">
                    Please check your email to reset your password.
                </p>
            </asp:PlaceHolder>
        </div>
    </div>
    <script>
        window.onload = function () {
            Validate("Email", "RegistrationKey", "Password", "", "SubmitButton", 3);
        }; 
    </script>
</asp:Content>

