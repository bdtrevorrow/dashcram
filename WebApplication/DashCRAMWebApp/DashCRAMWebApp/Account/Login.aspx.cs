﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using DashCRAMWebApp.Models;
using System.Web.Security;

namespace DashCRAMWebApp.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/MyVideos.aspx");
            }

            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }


        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                //var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                using (var db = new dashcramdbEntities())
                {
                    if (ValidateLogin(Email.Text, Password.Text))
                    {
                        //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        //Session["user"] = Email.Text;
                        //FormsAuthentication.SetAuthCookie(Email.Text, true);
                        bool rememberMe = RememberMe.Checked;
                        FormsAuthentication.RedirectFromLoginPage(Email.Text, rememberMe);
                        //Response.Redirect("../MyVideos.aspx");
                    }
                    else
                    {
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                    }
                }
            }
        }

        // Given a username/password, checks to make sure it exists in the db
        public static bool ValidateLogin(string username, string password)
        {
            using (var db = new dashcramdbEntities())
            {
                account account = db.accounts.Find(username);

                return account != null && account.password == password;
            }
        }
    }
}