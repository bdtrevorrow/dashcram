﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="DashCRAMWebApp.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <!--<asp:Literal runat="server" ID="ErrorMessage" />-->
    </p>
    <style>

        /*hide validation message fields*/

        #message, #message2, #message3 {

            display: none;
        }

        /* Add a green text color and a checkmark when the requirements are right */
        .valid {
            color: green;
        }

            .valid:before {
                position: relative;
                left: -35px;
                content: "✔";
            }

        /* Add a red text color and an "x" when the requirements are wrong */
        .invalid {
            color: red;
        }

            .invalid:before {
                position: relative;
                left: -35px;
                content: "✖";
            }
    </style>
    <script src="../Scripts/validate.js"></script>

    <div class="form-horizontal">
        <h4>Create a new account</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <asp:PlaceHolder runat="server" Visible="true">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Username</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" ClientIDMode="Static" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="The username field is required." />
                <!--<asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="The email field is required." />-->
            </div>
        </div>

          <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Registration Key</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="RegistrationField" ClientIDMode="Static" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="RegistrationField" 
                                CssClass="text-danger" ErrorMessage="The registration key field is required." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" ClientIDMode="Static" TextMode="Password" CssClass="form-control" />
                <!--<asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="The password field is required." /> -->
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" ClientIDMode="Static" TextMode="Password" CssClass="form-control" />
                <!--<asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." /> -->
            </div>
        </div>
        <div id="errMessage">
            <label id="invalidPi" runat="server"></label>
        </div>
        <div id="message">
            <h3>Password must contain the following:</h3>
            <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
            <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
            <p id="number" class="invalid">A <b>number</b></p>
            <p id="length" class="invalid">Minimum <b>8 characters</b></p>
        </div>
        <div id="message2">
            <h3>Password must contain the following:</h3>
            <p id="identical" class="invalid">Passwords must match</p>
        </div>
         <div id="message3">
            <h3>Registration key must contain the following:</h3>
            <p id="id" class="invalid">Must be a valid Raspberry Pi ID</p>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="RegisterButton" ClientIDMode="Static" runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" Disabled="true"/>
            </div>
        </div>
    </div>
    <script>
        window.onload = function () {
            Validate("Email", "RegistrationField", "Password", "ConfirmPassword", "RegisterButton", 2);
        }; 
    </script>
</asp:Content>

