﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using DashCRAMWebApp.Models;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DashCRAMWebApp.Account
{
    public partial class Manage : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorLabel.Visible = false;

            using (var db = new dashcramdbEntities())
            {
                var cams = db.cams.Where(x => x.username == HttpContext.Current.User.Identity.Name);

                foreach(var cam in cams)
                {
                    Label PiLabel = new Label();
                    Button DeleteButton = new Button();
                    PiLabel.Text = cam.pi_id;
                    DeleteButton.ID = cam.pi_id;
                    DeleteButton.Click += DeletePiButton_Click;
                    DeleteButton.Text = "Remove Camera";
                    DeleteButton.CssClass = "btn btn-default";
                    PisDisplay.Controls.Add(PiLabel);
                    PisDisplay.Controls.Add(DeleteButton);
                    PisDisplay.Controls.Add(new LiteralControl("<br/>"));
                }
            }
        }

        protected void AddPiButton_Click(object sender, EventArgs e)
        {

            if (DashCRAMWebApp.Validate.IsPiTaken(NewPieKey.Text)) {
                ErrorLabel.InnerText = "Invalid Dash Cam Number. Please Try again";
                ErrorLabel.Visible = true;
                return;
            }

            if (!DbLevelAccountManager.AddPiToExisingAccount(HttpContext.Current.User.Identity.Name, NewPieKey.Text))
            {
                ErrorLabel.Visible = true;
                return;
            }

            PisDisplay.Controls.Clear();
            Page_Load(sender, e);
        }

        protected void DeletePiButton_Click(object sender, EventArgs e)
        {
            string id = ((Button)sender).ID;
            DbLevelAccountManager.RemovePiFromAccount(HttpContext.Current.User.Identity.Name, id);
            PisDisplay.Controls.Clear();
            Page_Load(sender, e);
        }
    }
}