﻿<%@ Page Title="Manage Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="DashCRAMWebApp.Account.Manage" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <style>

        /*hide validation message fields*/
        #ValidateMessage {
            display: none;
        }

        /* Add a green text color and a checkmark when the requirements are right */
        .valid {
            color: green;
        }

            .valid:before {
                position: relative;
                left: -35px;
                content: "✔";
            }

        /* Add a red text color and an "x" when the requirements are wrong */
        .invalid {
            color: red;
        }

            .invalid:before {
                position: relative;
                left: -35px;
                content: "✖";
            }
    </style>
    <script src="../Scripts/validate.js"></script>

   <p> Manage Camera Registration Keys </p>

    <asp:TextBox runat="server" id="NewPieKey" ClientIDMode="Static" />
    <asp:Button ID="AddPiButton" OnClick="AddPiButton_Click" ClientIDMode="Static" runat="server" Text="Add Key" CssClass="btn btn-default" Disabled="true"/><br />
    <div id="ValidateMessage">
            <h3>Registration key must contain the following:</h3>
            <p id="id" class="invalid">Must be a valid Raspberry Pi ID</p>
    </div>
    <label id="ErrorLabel" style="color:red" runat="server">This Dash Cam Does Not Exist</label>
 
    <br />
    <div id="PisDisplay" runat="server">

    </div>
     <script>
        window.onload = function () {
            ValidatePiId("NewPieKey", "AddPiButton");
        }; 
    </script>
   

</asp:Content>
