﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using DashCRAMWebApp.Models;
using System.Web.Security;

namespace DashCRAMWebApp.Account
{
    public partial class Register:Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/MyVideos.aspx");
            }
        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
                if(DashCRAMWebApp.Validate.PiRegKey(RegistrationField.Text) == false) {
                    FailureText.Text = "Invalid Registration number";

                } else if(DashCRAMWebApp.Validate.UserName(Email.Text)) {

                    FailureText.Text = "This Account Already Exists";

                } else if(DashCRAMWebApp.Validate.IsPiTaken(RegistrationField.Text)) {

                    FailureText.Text = "That camera is already registered to another account";

                } else if(DbLevelAccountManager.AddAccountToDb(RegistrationField.Text,Email.Text,Password.Text) == false) {

                    FailureText.Text = "Error Creating Account\n Please Try Again";

                } else {

                // ErrorMessage.Text = result.Errors.FirstOrDefault();
                FormsAuthentication.RedirectFromLoginPage(Email.Text, false);

                return;
                }

                ErrorMessage.Visible = true;
        }
    }
}