﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using DashCRAMWebApp.Models;
using DashCRAMWebApp;

namespace DashCRAMWebApp.Account
{
    public partial class ForgotPassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Forgot(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user's email address
                //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                //ApplicationUser user = manager.FindByName(Email.Text);
                //if (user == null || !manager.IsEmailConfirmed(user.Id))
                if(DashCRAMWebApp.Validate.UserName(Email.Text) == false)
                {
                    FailureText.Text = "The user either does not exist or is not confirmed.";
                    ErrorMessage.Visible = true;
                    return;
                }
                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send email with the code and the redirect to reset password page
                //string code = manager.GeneratePasswordResetToken(user.Id);
                //string callbackUrl = IdentityHelper.GetResetPasswordRedirectUrl(code, Request);
                //manager.SendEmail(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>.");
                // loginForm.Visible = false;
                // DisplayEmail.Visible = true;

                // TODO: ResetPassword.ResetPwd // use this to reset the pass word and use your stuff to validate username and pi

                if(DashCRAMWebApp.Validate.PiAccount(RegistrationKey.Text,Email.Text) == false) {

                    FailureText.Text = "Registration Key Is Invalid";
                    ErrorMessage.Visible = true;
                    RegistrationKey.Text = "";
                    Email.Text = "";
                    // This isn't redirecting back to itself TODO: FIX ME!
                    //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"],Response);
                    //Response.Redirect("Forgot.aspx");
                    return;

                } else {

                    DbLevelAccountManager.ResetPwd(Email.Text,Password.Text);
                    Response.Redirect("~/Account/Login.aspx");

                }

            }
        }
    }
}

