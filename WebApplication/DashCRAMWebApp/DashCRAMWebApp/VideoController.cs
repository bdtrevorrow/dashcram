﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.IO;

namespace DashCRAMWebApp
{
    public class VideoController : ApiController
    {
        // POST api/<controller>
        [HttpPost]
        public string Post(string pi_key,DateTime indate, TimeSpan intime, [FromUri]byte[] video)
        {
            // Get the video's bytes as a stream
            var requestStream = Request.Content.ReadAsStreamAsync().Result;

            using (var db = new dashcramdbEntities())
            {
                // Find the user associated with the pi
                var account = db.cams.Find(pi_key);
                if (account == null) return "That pi_key is not associated with an account";
                string username = account.username;

                // Get their current number of videos
                int numOfVideos = db.videos.Where(x => x.username == username).Count();
                // Get the spot the video will be stored in
                // Names the video with 1 number higher than how many vids the user has
                string file_location = AppDomain.CurrentDomain.BaseDirectory + "/Content/Videos/" + username + "/";
                string file_name = "video" + DateTime.Now.Ticks + ".mp4";
                // If the directory for the user doesn't exist, create it
                if (!Directory.Exists(file_location)) Directory.CreateDirectory(file_location);

                // Store the video file 
                using (FileStream fileStream = File.Create(file_location + file_name))
                {
                    requestStream.CopyTo(fileStream);
                    requestStream.Close();
                }

                // Store the video location in the db
                db.videos.Add(new video
                {
                    username = account.username,
                    date = indate,
                    time = intime,
                    video_name = file_name,
                });

                db.SaveChanges();   // Commit the changes to the db
            }

            return "File stored successfully!";
        }

        // GET api/<controller>
        [HttpGet]
        public string Get()
        {
            return "This just makes sure the Video controller is accessible from the browser";
        }

    }
}