﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DashCRAMWebApp.Startup))]
namespace DashCRAMWebApp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
