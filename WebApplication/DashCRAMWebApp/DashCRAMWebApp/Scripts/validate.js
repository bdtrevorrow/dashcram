﻿//password validation: id = password text field id, type: 1 = login check, 2 = account creation check 3 = forgot password
function Validate(unId, piId, pwId, pwId2, button, type) {
    console.log("javascript executed");

    //get username and password form
    var un = document.getElementById(unId);
    var pw = document.getElementById(pwId);
    var pw2;
    var pi;
    //get password confirm form
    if (type == 2) {
        pw2 = document.getElementById(pwId2);
    }
    //get pi id form
    if (type == 2 || type == 3) {
        pi = document.getElementById(piId);
    }

    //Validation pop-up lists
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var length = document.getElementById("length");
    var identical;
    var id;
    //get password confirm pop-up list
    if (type == 2) {
        identical = document.getElementById("identical");
    }
    //get pi id pop-up list
    if (type == 2 || type == 3) {
        id = document.getElementById("id");
    }

    //Booleans for for form completion checks
    var unNotEmpty   = false;
    var hasLowercase = false;
    var hasUppercase = false;
    var hasNumber    = false;
    var hasLength    = false;
    var match;
    var validId;
    //Boolean values for different validation types
    if (type == 2) {
        match = false;
        validId = false;
    } else if (type == 3) {
        match = true;
        validId = false;
    } else {
        match = true;
        validId = true;
    }

    //If user clicks on password field show message
    pw.onfocus = function () {
        document.getElementById("message").style.display = "block";
    }

    //if user clicks away from password field hide message
    pw.onblur = function () {
        document.getElementById("message").style.display = "none";
    }

    if (type == 2) {
        //If user clicks on password2 field show message
        pw2.onfocus = function () {
            document.getElementById("message2").style.display = "block";
        }

        //if user clicks away from password2 field hide message
        pw2.onblur = function () {
            document.getElementById("message2").style.display = "none";
        }

    } 

    if (type == 2 || type == 3) {
        //If user clicks on pi id field show message
        pi.onfocus = function () {
            document.getElementById("message3").style.display = "block";
        }

        //if user clicks away from pi id field hide message
        pi.onblur = function () {
            document.getElementById("message3").style.display = "none";
        }
    }
    //checks if forms are already filled out due to previous faile login attempt
    if (validUN(document.getElementById(unId).value)) {
        unNotEmpty = true;
    }
    if (type == 2 || type == 3) {
        if (validPiID(document.getElementById(piId).value)) {
            validId = true
            id.classList.remove("invalid")
            id.classList.add("valid");
        }
    }

    //Event listener
    document.addEventListener("keyup", function (e) {
        //Get the element
        var element = e.target;

        //Check if element is username text box
        if (element === un) {
            if (validUN(element.value)) {
                unNotEmpty = true;
            } else {
                unNotEmpty = false;
            }
        }
        //Check if element is pi id text box
        if (type == 2 || type == 3) {
            if (element === pi) {
                if (validPiID(element.value)) {
                    id.classList.remove("invalid")
                    id.classList.add("valid");
                    validId = true;
                } else {
                    id.classList.remove("valid");
                    id.classList.add("invalid");
                    validId = false;
                }
            }
        }

        //check if element is password text box
        if (element === pw) {
            //validate lowercase letters
            if (validLowerCase(element.value)) {
                letter.classList.remove("invalid");
                letter.classList.add("valid");
                hasLowercase = true;
            } else {
                letter.classList.remove("valid");
                letter.classList.add("invalid");
                hasLowercase = false;
            }
            //Validate capital letters
            if (validUpperCase(element.value)) {
                capital.classList.remove("invalid");
                capital.classList.add("valid");
                hasUppercase = true;
            } else {
                capital.classList.remove("valid");
                capital.classList.add("invalid");
                hasUppercase = false;
            }
            //Validate numbers
            if (validNumbers(element.value)) {
                number.classList.remove("invalid");
                number.classList.add("valid");
                hasNumber = true;
            } else {
                number.classList.remove("valid");
                number.classList.add("invalid");
                hasNumber = false;
            }
            //Validate length
            if (validLength(element.value)) {
                length.classList.remove("invalid");
                length.classList.add("valid");
                hasLength = true;
            } else {
                length.classList.remove("valid");
                length.classList.add("invalid");
                hasLength = false;
            }
        }

        //check if element is password validation box
        if (type == 2) {
            if (element === pw2 || element === pw) {
                if (passwordsMatch(document.getElementById(pwId).value, document.getElementById(pwId2).value)) {
                    identical.classList.remove("invalid");
                    identical.classList.add("valid");
                    match = true;
                } else {
                    identical.classList.remove("valid");
                    identical.classList.add("invalid");
                    match = false;
                }
            }
        }

        console.log(hasLowercase + ", " + hasUppercase + ", " + hasNumber + ", " + hasLength + ", " + unNotEmpty + ", " + match + ", " + validId);
        //Validate form completion and enable button
        if (hasLowercase && hasUppercase && hasNumber && hasLength && unNotEmpty && match && validId) {
            if (document.getElementById(button).disabled != false) {
                document.getElementById(button).disabled = false;
            }
        } else {
            if (document.getElementById(button).disabled != true) {
                document.getElementById(button).disabled = true;
            }
        }
    })
}



//returns true if string is not empty
function validUN(username) {
    var whitespace = /\s/g;
    if (whitespace.test(username)) {
        return false;
    } else {
        if (username != "") {
            return true;
        } else {
            return false;
        }
    }
}

//returns true if string is a 16 digit hex number
function validPiID(piID) {
    var validPiId = /^[a-f\d]{16}$/gi;

    if (piID.match(validPiId)) {
        return true;
    } else {
        return false;
    }
}

//returns true if string has a lowercase letter
function validLowerCase(password) {
    var lowerCaseLetters = /[a-z]/g;

    if (password.match(lowerCaseLetters)) {
        return true;
    } else {
        return false;
    }
}

//returns true if string has an uppercase letter
function validUpperCase(password) {
    var upperCaseLetters = /[A-Z]/g;

    if (password.match(upperCaseLetters)) {
        return true;
    } else {
        return false;
    }
}

//returns true if string has a number
function validNumbers(password) {
    var numbers = /[0-9]/g;

    if (password.match(numbers)) {
        return true;
    } else {
        return false;
    }
}

//returns true if string is >= 8
function validLength(password) {
    if (password.length >= 8) {
        return true;
    } else {
        return false;
    }
}

//returns true if both strings are equal
function passwordsMatch(password1, password2) {
    if (password1 != "" || password2 != "") {
        if (password1 == password2) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function ValidatePiId(piId, button) {
    //get pi id form
    var idForm = document.getElementById(piId);
    //get pi id validation message
    var validateMessage = document.getElementById("ValidateMessage");
    //boolean for valid pi id
    var validPiId = false;
    //if user clicks on pi id field show message
    idForm.onfocus = function () {
        document.getElementById("ValidateMessage").style.display = "block";
    }
    //if user clicks away from pi id field hide message
    idForm.onblur = function () {
        document.getElementById("ValidateMessage").style.display = "none";
    }
    //Event listener
    document.addEventListener("keyup", function (e) {
        //Get the element
        var element = e.target;
        //if element is pi id form
        if (element === idForm) {
            if (validPiID(element.value)) {
                id.classList.remove("invalid")
                id.classList.add("valid");
                validPiId = true;
            } else {
                id.classList.remove("valid")
                id.classList.add("invalid");
                validPiId = false;
            }
        }
        //activate/deactivate button
        if (validPiId) {
            if (document.getElementById(button).disabled != false) {
                document.getElementById(button).disabled = false;
            }
        } else {
            if (document.getElementById(button).disabled != true) {
                document.getElementById(button).disabled = true;
            }
        }
    })
}