﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DashCRAMWebApp
{
    public partial class MyVideos : System.Web.UI.Page
    {
        List<video> videos = new List<video>();
        Dictionary<String, DataTable> myFolders;
        protected void Page_Load(object sender, EventArgs e)
        {
            // If page is loading for the first time, get videos and store them in the viewstate
            if (!Page.IsPostBack)
            {
                Random random = new Random();
                String user = HttpContext.Current.User.Identity.Name;

                videos = DbLevelAccountManager.RetrieveUserVideos(user);
                /*
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        video vid = new video();
                        vid.id = random.Next();
                        vid.date = DateTime.Now;
                        vid.video_folder = "folder" + i.ToString();
                        vid.time = TimeSpan.Zero;
                        vid.video_name = "test" + j.ToString();
                        vid.username = "test";
                        videos.Add(vid);
                    }
                }
                */
                ViewState["videos"] = videos;
                if (videos.Count > 0)
                {
                    myFolders = GetDataTables(videos);
                    ViewState["folders"] = myFolders;
                    BindData();
                    noVids.InnerHtml = "";
                }
                else
                {
                    noVids.InnerHtml = "You have no videos.";
                }
            }
            // If reloaded by postback, retrieve videos from ViewState
            else
            {
                videos = (List<video>)ViewState["videos"];
                myFolders = (Dictionary<String, DataTable>)ViewState["folders"];
            }

        }

        protected Dictionary<String, int> GetFolders(List<video> vids)
        {
            // Dictionary with folder name and number of videos in the folder
            Dictionary<String, int> dictionary = new Dictionary<string, int>();
            // Main folder should always exist, even if empty
            if (!FolderList.Items.Contains(new ListItem("Main", "Main")))
            {
                FolderList.Items.Add(new ListItem("Main", "Main"));
                MoveFolderList.Items.Add(new ListItem("Main", "Main"));
            }
            dictionary.Add("Main", 0);

            // Get folder names by looping through the video list
            foreach (video vid in vids)
            {
                // If folder property of the video has the default null value, display the video in the "Main" folder
                if (vid.video_folder == null)
                {
                    // Check to see if the Main folder has been added to the dictionary already, if so add 1 to the value
                    if (dictionary.ContainsKey("Main"))
                    {
                        dictionary["Main"] += 1;
                    }
                    // Else, check to see if the FolderList DropDownList already has the Main folder and add it if not, and then add Main to the dictionary
                    else 
                    {
                        if (!FolderList.Items.Contains(new ListItem("Main", "Main")))
                        {
                            FolderList.Items.Add(new ListItem("Main", "Main"));
                            MoveFolderList.Items.Add(new ListItem("Main", "Main"));

                        }
                        dictionary.Add("Main", 1);
                    }
                }
                // Video does not have default null value, so check to see if the folder it's in has already been added to the dictionary
                else if (dictionary.ContainsKey(vid.video_folder))
                {
                    dictionary[vid.video_folder] += 1;
                }
                // Video does not have the default null value and its folder has not been added to the dictionary yet, so add it to the FolderList and the dictionary.
                else
                {
                    if (!FolderList.Items.Contains(new ListItem(vid.video_folder, vid.video_folder)))
                    {
                        FolderList.Items.Add(new ListItem(vid.video_folder, vid.video_folder));
                        MoveFolderList.Items.Add(new ListItem(vid.video_folder, vid.video_folder));
                        DeleteFolderList.Items.Add(new ListItem(vid.video_folder, vid.video_folder));
                    }
                    dictionary.Add(vid.video_folder, 1);
                }
            }
            // Bind the items to the FolderList
            FolderList.DataBind();
            MoveFolderList.DataBind();
            DeleteFolderList.DataBind();
            return dictionary;
        }

        protected Dictionary<String, DataTable> GetDataTables(List<video> vids)
        {
            // Dictionary with folder name and number of videos in the folder
            Dictionary<String, int> dictionary = new Dictionary<string, int>();

            dictionary = GetFolders(vids);
            

            // Create the DataTables for each folder and put them in a dictionary
            // Key is folder name, Value is DataTable with videos in that folder
            Dictionary<String, DataTable> myTables = new Dictionary<string, DataTable>();
            // Loop through each key-value pair 
            foreach (KeyValuePair<String, int> pair in dictionary)
            {
                // Add new folder to the dictionary with a null value
                myTables.Add(pair.Key, null);
                // Create DataTable to add to the dictionary key
                DataTable dt = new DataTable();
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("Title", typeof(string));
                dt.Columns.Add("dateTaken", typeof(string));
                dt.Columns.Add("timeTaken", typeof(string));
                dt.Columns.Add("checked", typeof(bool));


                //Get videos with this folder into a datatable
                foreach (video vid in vids)
                {
                    if (vid.video_folder == pair.Key || (vid.video_folder == null && pair.Key == "Main"))
                    {
                        DataRow row = dt.NewRow();
                        row["ID"] = vid.id;
                        row["Title"] = vid.video_name;
                        row["dateTaken"] = CheckNullDate(vid.date).Value.ToShortDateString();
                        row["timeTaken"] = CheckNullTime(vid.time).ToString();
                        row["checked"] = true;
                        dt.Rows.Add(row);
                    }
                }
                // Put the created DataTable into the appropriate folder key in the dictionary
                myTables[pair.Key] = dt;
            }
            return myTables;

        }

        public void BindData()
        {
            // Search for the correct DataTable to display with the folder
            foreach (KeyValuePair<String, DataTable> pair in myFolders)
            {
                if (FolderList.SelectedValue == pair.Key)
                {
                    videosGrid.DataSource = pair.Value;
                    videosGrid.DataBind();
                    return;
                }

            }
            // Folder is empty, display nothing
            videosGrid.DataSource = null;
            videosGrid.DataBind();
        }

        // Event that fires when the folder selected in the FolderList dropdownlist changes
        protected void folderSelected(object sender, EventArgs e)
        {
            // Search for the correct DataTable to display with the folder
            foreach (KeyValuePair<String, DataTable> pair in myFolders)
            {
                if (FolderList.SelectedValue == pair.Key)
                {
                    videosGrid.DataSource = pair.Value;
                    videosGrid.DataBind();
                    noVids.InnerText = "";
                    return;
                }
            }
            // Folder is empty, display nothing
            videosGrid.DataSource = null;
            videosGrid.DataBind();
        }

        protected void videosGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "select")
            {
                int index = Convert.ToInt32(e.CommandArgument);

                //var clickedVideoPath = videosGrid.DataKeys[index].Value.ToString();
                var clickedVideoID = videosGrid.DataKeys[index].Value.ToString();

                //Session["SelectedVideo"] = clickedVideoID;
                Response.Redirect("ViewVideo.aspx?videoID=" + clickedVideoID);
            }
        }

        protected void videosGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Get ID of video to be deleted
            int videoID = Convert.ToInt32(videosGrid.DataKeys[e.RowIndex].Value);
            videos.Remove(videos.Single(x => x.id == videoID));
            UpdateViewState();
            // Delete file from server and record in DB
            DbLevelAccountManager.RemoveVideo(videoID);

        }

        protected DateTime? CheckNullDate(DateTime? vidDate)
        {
            if (vidDate == null)
            {
                vidDate = DateTime.Now;
            }
            return vidDate;
        }

        protected TimeSpan? CheckNullTime(TimeSpan? vidTime)
        {
            if (vidTime == null)
            {
                vidTime = TimeSpan.Zero;
            }
            return vidTime;
        }

        protected void newFolderButton_Click(object sender, EventArgs e)
        {
            // Make sure input is not empty
            if (!String.IsNullOrEmpty(folderNameInput.Text))
            {
                // Check to see if a folder with the name exists already
                if (!FolderList.Items.Contains(new ListItem(folderNameInput.Text, folderNameInput.Text)))
                {
                    // Add new empty folder to the two dropdown lists
                    FolderList.Items.Add(new ListItem(folderNameInput.Text, folderNameInput.Text));
                    MoveFolderList.Items.Add(new ListItem(folderNameInput.Text, folderNameInput.Text));
                    DeleteFolderList.Items.Add(new ListItem(folderNameInput.Text, folderNameInput.Text));
                }
            }
        }

        protected void DeleteSelected_Click(object sender, EventArgs e)
        {
            bool atLeastOneRowDeleted = false;
            List<int> vidsToDelete = new List<int>();
            // Loop through each row in the current folder
            foreach(GridViewRow row in videosGrid.Rows)
            {
                // Check to see if the user selected the video
                CheckBox cb = (CheckBox)row.FindControl("VideoSelector");
                if (cb != null && cb.Checked)
                {
                    // Set flag that a video will be deleted
                    atLeastOneRowDeleted = true;
                    // Get the ID and add it to the list of IDs
                    int videoID = Convert.ToInt32(videosGrid.DataKeys[row.RowIndex].Value);
                    vidsToDelete.Add(videoID);
                    // Remove the video from the front end table and videos list
                    //videosGrid.DeleteRow(row.RowIndex);
                    videos.Remove(videos.Single(x => x.id == videoID));
                }
            }
            // At least one video will be deleted
            if (atLeastOneRowDeleted)
            {
                UpdateViewState();
                // Use list of IDs to delete from DB
                foreach(int ID in vidsToDelete)
                {
                    DbLevelAccountManager.RemoveVideo(ID);
                }
            }
        }

        protected void VideoSelector_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected void deleteFolderButton_Click(object sender, EventArgs e)
        {
            // List of IDs of videos that will need to be deleted
            List<int> vidsToDelete = new List<int>();

            bool isFolderEmpty = true;

            // Folder that the user selected in the dropdown list
            var folderToBeDeleted = DeleteFolderList.SelectedValue;

            //Remove folder from the dropdown lists
            FolderList.Items.Remove(new ListItem(folderToBeDeleted, folderToBeDeleted));
            MoveFolderList.Items.Remove(new ListItem(folderToBeDeleted, folderToBeDeleted));
            DeleteFolderList.Items.Remove(new ListItem(folderToBeDeleted, folderToBeDeleted));


            // Check to see if the user is currently in the folder they're trying to delete
            // If so, change the opened folder to Main, which can't be deleted
            if (folderToBeDeleted == FolderList.SelectedValue)
            {
                FolderList.SelectedValue = "Main";
                folderSelected(null, null);
            }
            
            // Get the ids of all videos in that folder
            foreach (video vid in videos)
            {
                if (vid.video_folder == folderToBeDeleted)
                {
                    isFolderEmpty = false;
                    vidsToDelete.Add(vid.id);
                }
            }

            // Folder was not empty, so 
            //      Remove the videos from the local videos list
            //      Remove its data table from the dictionary
            //      Update ViewState variables
            //      Bind the updated data
            if (!isFolderEmpty)
            {
                // Delete videos from 
                foreach (int deleteID in vidsToDelete)
                {
                    videos.Remove(videos.Single(x => x.id == deleteID));
                }
                // Remove the folder from the folders dictionary
                myFolders.Remove(folderToBeDeleted);

                UpdateViewState();

                // Use list of IDs to delete from DB
                foreach (int ID in vidsToDelete)
                {
                    DbLevelAccountManager.RemoveVideo(ID);
                }
            }

        }

        protected void moveSelectedButton_Click(object sender, EventArgs e)
        {
            // Folder that the user selected in the dropdown list
            var folderDestination = MoveFolderList.SelectedValue;
            // Make sure the destination folder isn't the currently opened folder
            if (folderDestination != FolderList.SelectedValue)
            {
                bool atLeastOneRowMoved = false;
                List<int> vidsToMove = new List<int>();
                // Loop through each video in the current folder
                foreach (GridViewRow row in videosGrid.Rows)
                {
                    // Check to see if the video was selected
                    CheckBox cb = (CheckBox)row.FindControl("VideoSelector");
                    if (cb != null && cb.Checked)
                    {
                        // Set flag that a video will be moved
                        atLeastOneRowMoved = true;
                        // Add ID of video to be moved to list of IDs of videos to be moved
                        int videoID = Convert.ToInt32(videosGrid.DataKeys[row.RowIndex].Value);
                        vidsToMove.Add(videoID);
                        // Remove the video from the current table for the current folder
                        //videosGrid.DeleteRow(row.RowIndex);
                        // Check to see if the folder destination is Main, if it is, set the folder to null
                        if (folderDestination != "Main")
                        {
                            videos.Find(x => x.id == videoID).video_folder = folderDestination;
                        }
                        else
                        {
                            videos.Find(x => x.id == videoID).video_folder = null;
                        }
                    }
                }
                // A video is going to be moved to a different folder
                if (atLeastOneRowMoved)
                {
                    // If the destination is Main, set the destination to null
                    if (folderDestination == "Main")
                    {
                        folderDestination = null;
                    }
                    UpdateViewState();
                    // Use list of IDs to change folder field in DB
                    DbLevelAccountManager.TagVidsToFolder(vidsToMove, folderDestination);
                }
            }
        }

        protected void UpdateViewState()
        {
            // Update ViewState videos and folders
            ViewState["videos"] = videos;
            myFolders = GetDataTables(videos);
            ViewState["folders"] = myFolders;
            // Bind the updated data
            BindData();
        }
    }
}
 