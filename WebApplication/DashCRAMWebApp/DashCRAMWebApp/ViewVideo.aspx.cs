﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DashCRAMWebApp
{
    public partial class ViewVideo : System.Web.UI.Page
    {

        video vid;

        protected void Page_Load(object sender, EventArgs e)
        {
            String user = HttpContext.Current.User.Identity.Name;
            //String selectedVid = Session["SelectedVideo"].ToString();
            String selectedVid = Request.QueryString["videoID"];
            vid = GetVideo(user, selectedVid);

            if (!Page.IsPostBack)
            {
                

                String path = GetPath(vid);
                video_source.Src = path;
                video_title.Text = stripExtension(vid.video_name);
                video_date.InnerText = vid.date.ToLocalTime().ToShortDateString();
                video_time.InnerText = vid.time.ToString(@"hh\:mm\:ss");
            }
        }

        protected string stripExtension(string vidName)
        {
            string noExtension = System.IO.Path.ChangeExtension(vidName, null);
            return noExtension;
        }

        protected video GetVideo(String user, String ID)
        {
            try
            {
                var DbContext = new dashcramdbEntities();
                var video = DbContext.videos.Where(x => x.username == user && x.id.ToString() == ID).First();
                return video;
            }
            catch
            {
                Response.Redirect("Account/Login.aspx");
                return null;
            }
        }

        protected String GetPath(video video)
        {
            String path;
            path = "~/Content/Videos/" + video.username + "/" + video.video_name;
            return path;
        }

        protected void editTitleButton_Click(object Sender, EventArgs arrgs) {

            DbLevelAccountManager.ChangeVideoName(HttpContext.Current.User.Identity.Name, vid.video_name, System.IO.Path.ChangeExtension(video_title.Text, ".mp4"));
            vid.video_name = System.IO.Path.ChangeExtension(video_title.Text, ".mp4");
            video_source.Src = GetPath(vid);
        }
    }
}