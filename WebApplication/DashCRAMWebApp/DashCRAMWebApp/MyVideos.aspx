﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyVideos.aspx.cs" Inherits="DashCRAMWebApp.MyVideos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>My Videos</h2>

    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            Select folder:&nbsp;&nbsp;
            <asp:DropDownList ID="FolderList" runat="server" OnSelectedIndexChanged="folderSelected" AutoPostBack="true">
            </asp:DropDownList>
            <br />
            New Folder Name:
            <asp:TextBox ID="folderNameInput" runat="server"></asp:TextBox>

            <asp:Button ID="Button1" runat="server" OnClick="newFolderButton_Click" Text="Create New Folder" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="DeleteFolder" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <h3 id="noVids" runat="server"></h3>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="videosGrid" runat="server" Width="800px" AutoGenerateColumns="False" OnRowCommand="videosGrid_RowCommand" DataKeyNames="ID"
                BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                OnRowDeleting="videosGrid_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                    <asp:ButtonField DataTextField="Title" CommandName="select" HeaderText="Title">
                        <ItemStyle Font-Underline="True" />
                    </asp:ButtonField>
                    <asp:BoundField DataField="dateTaken" HeaderText="Date" />
                    <asp:BoundField DataField="timeTaken" HeaderText="Time" />
                    <asp:CommandField DeleteText="Delete Video" HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True">
                        <ItemStyle Font-Underline="True" />
                    </asp:CommandField>
                    <asp:TemplateField HeaderText="Selected">
                        <ItemTemplate>
                            <asp:CheckBox ID="VideoSelector" runat="server" AutoPostBack="true" OnCheckedChanged="VideoSelector_CheckedChanged" />
                            <asp:HiddenField runat="server" ID="selectedValue" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                <RowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="FolderList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="DeleteSelected" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="MoveSelected" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="DeleteFolder" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="videosGrid" EventName="RowEditing" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="DeleteSelected" runat="server" OnClick="DeleteSelected_Click" Text="Delete Selected Videos" />
    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            Folder to move selected videos to:
            <asp:DropDownList ID="MoveFolderList" runat="server" AutoPostBack="true">
            </asp:DropDownList>
            <asp:Button ID="MoveSelected" runat="server" OnClick="moveSelectedButton_Click" Text="Move Videos" />
            <br />
            <br />
            Folder to Delete:
            <asp:DropDownList ID="DeleteFolderList" runat="server" AutoPostBack="true">
            </asp:DropDownList>
            <asp:Button ID="DeleteFolder" runat="server" OnClick="deleteFolderButton_Click" Text="Delete Folder" 
                OnClientClick="return confirm('Are you sure you want to delete this folder and all of its contents?');"/>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="DeleteFolder" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
