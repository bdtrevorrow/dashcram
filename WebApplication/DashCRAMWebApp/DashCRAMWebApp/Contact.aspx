﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="DashCRAMWebApp.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Contact Team DASH CRAM</h2>
    <address>
        450 School House Rd<br />
        Johnstown, PA 15901<br />
        <abbr title="Phone">P:</abbr>
        1-800-DASH-CRAM
    </address>

    <address>
        <strong>Support:</strong>   <a href="mailto:Support@example.com">support@dashcram.com</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@dashcram.com</a>
    </address>
</asp:Content>
