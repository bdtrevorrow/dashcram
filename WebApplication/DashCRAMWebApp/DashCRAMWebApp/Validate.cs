﻿using System;
using System.Linq;

namespace DashCRAMWebApp{
    public static class Validate{

        public static bool PiRegKey(String userEnteredPiKey) {

            // This Method Looks to see if a pi key parameter matches pi keys that are in the Database
            // Returns true is a match is found
            // Returns false if no match is found

            // Passes Unit Testing
            var DbContext = new dashcramdbEntities();
            var item = DbContext.cams.Where(x => x.pi_id == userEnteredPiKey).ToList();
            return item.Count > 0;

        }// end PiRegKey

        public static bool UserName(String userEnteredUserName) {

            // This Method Looks to see if UserEnteredUserName matches a user name in the DataBase
            // Returns true if match is found
            // Returns false if no match is found

            // Passes Unit Testing
            var DbContext = new dashcramdbEntities();
            var item = DbContext.accounts.Where(x => x.username == userEnteredUserName).ToList();
            return item.Count > 0;
        }

        public static bool IsPiTaken(String UserEnteredPiKey) {

            // This Method Checks to see if a pi has been registered to a user account
            // This is done by checking if the Both Columns in the table are populated in each row
            // Returns true if the Pi IS registered to an account
            // Returns false if the Pi is NOT registered to an account or the Pi Key does not exist 

            // Passes Unit Testing
            if(PiRegKey(UserEnteredPiKey) == false) return false; 
            var DbContext = new dashcramdbEntities();
            var item = DbContext.cams.Where(x => x.pi_id == UserEnteredPiKey && x.username != null).ToList();
            return item.Count > 0;
        }

        public static bool PiAccount(String userEnteredPiKey, String userEnteredUserName){

            if(PiRegKey(userEnteredPiKey) == false) return false;
            var DbContext = new dashcramdbEntities();
            var items = DbContext.cams.Where(x => x.pi_id == userEnteredPiKey && x.username == userEnteredUserName).ToList();
            return items.Count > 0;

        }
    }
}