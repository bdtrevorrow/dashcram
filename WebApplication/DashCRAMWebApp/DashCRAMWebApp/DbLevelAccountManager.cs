﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

// This class is for managing user accounts at the Database level

namespace DashCRAMWebApp{
    public static class DbLevelAccountManager{

        public static bool AddAccountToDb(String piKey, String userName, String passWord){

            // This method will add a NEW account to the Db.
            // Before an account is created within the Db this method will validate that the applicable user input is not in the Database
            // If the user entered data DOES NOT exist in the Database then the user data will then be written and a true value will be retuned
            // If the user data DOES exist then the method will return a false value and the data will not be written to the Database.
            // Returns true when user entered account data IS saved to the Db
            // Returns false when user account data is NOT saved to the Db

            // For Testing:
            //      Make sure the method returns false if a user name exists
            //      Make sure the method returns false if a pi key does not exist
            //      Make sure the method writes the correct test data to the Db and returns a true value

            if(Validate.UserName(userName))
                return false;       // If a user name exists then do not make a new account
            if(Validate.PiRegKey(piKey) == false)
                return false; // If a Pi Key does not exist do not make a new account
            if(Validate.IsPiTaken(piKey) == true)
                return false; // If a Pi is taken do not create a new account

            // modify account table -- Tested and working good!
            var DbContext = new dashcramdbEntities();
            var ttAccounts = new account { // Create a new row in the Account table and save applicable arguments to each column in the new row.
                username = userName,
                password = passWord
            };

            DbContext.accounts.Add(ttAccounts);
            DbContext.SaveChanges(); // end modifing Account table

            //modify the cam table --Tested and working good
            using(var db = new dashcramdbEntities()) {
                var result = db.cams.SingleOrDefault(b => b.pi_id == piKey);
                if(result != null) {
                    result.username = userName;
                    db.SaveChanges();
                } else {
                    return false;
                }
            }
            return true;
        }

        public static void ResetPwd(string username,string newPassword) {
            using(var db = new dashcramdbEntities()) {
                var cam = db.accounts.Find(username);
                cam.password = newPassword;
                db.SaveChanges();
            }
        }

        public static bool RemovePiFromAccount(String Username, String PiId) {

            using(var DbContext = new dashcramdbEntities()) {
                var result = DbContext.cams.SingleOrDefault(b => b.username == Username && b.pi_id == PiId);
                if(result != null) {
                    result.username = null;
                    DbContext.SaveChanges();
                    return true;
                }
            }

            return false;
        }

        public static bool AddPiToExisingAccount(String Username, String newpi) {

            //modify the cam table --Tested and working good
            using (var db = new dashcramdbEntities()) {
                var result = db.cams.SingleOrDefault(b => b.pi_id == newpi);
                if (result != null) {
                    result.username = Username;
                    db.SaveChanges();
                } else {
                    return false;
                }
            }
            return true;
        }

        public static bool ChangeVideoName(String userName, String currentVideoName, 
                                           String newVideoName, string file_location = "\\Content\\Videos\\"){

            file_location = AppDomain.CurrentDomain.BaseDirectory + file_location;
            file_location += userName + "\\";

            try
            {
                File.Move(file_location + currentVideoName, file_location + newVideoName);
            }
            catch(FileNotFoundException)
            {
                Console.WriteLine("Could not find " + file_location);
                return false;
            }

            // updating database to reflect the new file name
            using (var db = new dashcramdbEntities()) {
                var result = db.videos.SingleOrDefault(b => b.username == userName && b.video_name == currentVideoName);
                if (result != null) {
                    result.video_name = newVideoName;
                    db.SaveChanges();
                } else {
                    return false;
                }
            }
            return true;
        }

        public static List<video> RetrieveUserVideos(string username)
        {
            using (var db = new dashcramdbEntities())
            {
                var query = db.videos.Where(x => x.username == username);

                if (query != null) return query.ToList();
                else return null;
            }
        }

        public static bool TagVidsToFolder(List<int> RowIds, String folderName){ 

            foreach(int row in RowIds){ // loop through the list

                using(var db = new dashcramdbEntities()){ 

                    foreach(var Dbrow in db.videos){ // loop through the table                  

                        if(Dbrow.id == row) { // if the row id of the db == the int in the list
                                                          
                            Dbrow.video_folder = folderName; 
                           
                        }
                    }
                    db.SaveChanges(); // save the folder name to the folder column in the db

                }
            }
            return true;
        }

        public static bool RemoveVideo(int id, string file_location = "\\Content\\Videos\\")
        {
            file_location = AppDomain.CurrentDomain.BaseDirectory + file_location;

            try
            {
                using (var db = new dashcramdbEntities())
                {
                    var video = db.videos.Find(id);
                    File.Delete(file_location + video.username + "\\" + video.video_name);
                    db.videos.Remove(video);
                    db.SaveChanges();
                    return true;
                }
            }
            catch(FileNotFoundException)
            {
                Console.WriteLine("File not found");
                return false;
            }
            
        }
    }
}