﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="DashCRAMWebApp.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>What is Dash Cram?</h1>
    <h4>Dash Cram is a state of the art, motion detection based dash camera that will save your recordings in the cloud so you can view them
        from your desktop or mobile device at your leisure. Using this website you can create account to manage your videos. If you have a youtube live account,
        you're also able to stream your dash cam to your youtube account. The Pi's the limit with DASH CRAM!
    </h4>
</asp:Content>
